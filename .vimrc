" General settings

runtime! debian.vim
set nocompatible

" Set up Pathogen to manage plugins.
call pathogen#infect()

" Save one keystroke.
noremap ; :

" Improve copying and pasting.
set pastetoggle=<F2>
set clipboard=unnamed

" Rebind the <Leader> key.
let mapleader=','

" Use simpler key bindings to switch between windows.
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
map <C-h> <C-w>h

" Improve group indentation.
vnoremap < <gv
vnoremap > >gv

" Set up a color scheme.
set t_Co=256
set background=dark
let base16colorspace=256
colorscheme base16-monokai

" Enable syntax highlighting.
filetype off
filetype plugin indent on
syntax on
autocmd BufNewFile,BufRead *.hql set filetype=hive
autocmd BufNewFile,BufRead *.jinja,*.j2 set filetype=jinja

" Stop enforcing text wrapping.
set number
set tw=100
set nowrap
set fo-=t
highlight ColorColumn ctermbg=magenta ctermfg=white
call matchadd('ColorColumn', '\%100v', -1)

" Enable fast paragraph formatting.
vmap Q gq
nmap Q gqap
set nojoinspaces

" Use spaces instead of tabs.
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab

" Make search case insensitive.
set hlsearch
set incsearch
set ignorecase
set smartcase

" Disable stupid backup and swap files.
set nobackup
set nowritebackup
set noswapfile

" Configure folding.
set foldmethod=indent
set foldlevel=99
nnoremap <Space> za

" Remove trailing whitespaces automatically.
autocmd BufWritePre * :%s/\s\+$//e


" Plugin settings

" Configure vim-airline.
let g:airline_powerline_fonts=1
let g:airline_theme='base16'
set laststatus=2

" Configure ctrlp.vim.
let g:ctrlp_max_height=30
set wildignore+=*.pyc
noremap <Leader><Leader> :CtrlP<CR>
noremap <Tab> :CtrlPBuffer<CR>

" Configure syntastic.
let g:syntastic_check_on_open=1

" Configure YouCompleteMe.
nnoremap <leader>jd :YcmCompleter GoToDefinition<CR>
nnoremap <leader>jr :YcmCompleter GoToReferences<CR>
